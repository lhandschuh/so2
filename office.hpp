#include "include.hpp"
#include "resident.hpp"

#define MAX_COFFEE 100

#ifndef _OFFICE_HPP_
#define _OFFICE_HPP_ 

class Office{

private:

int floors;
int* seat;
bool lift;
int whereIsCoffee;
int storeroom;
int maxSeats;
int coffee;
list<short>* queue;
mutex store, window, dispenser, QQ, elevator;

public:

Office(int numberOfFloors, int numberOfSeats, int coffeeFloor);
string getQs(int floor);
string getCs(int floor);
bool serve(int floor);
void loadCoffee();
bool giveMeCoffee();
int checkStoreroom();
int checkCoffee();
void liftInUse();
void freeLift();
bool canIUseLift();
bool canIQ(int floor, short id);
void fillStoreroom(int coffee);
bool canISeat(int floor);
void releaseSeat(int floor);
int getFloors();
int getCoffeeFloor();
bool stillInQ(short id, int floor);

};

#endif