#include "resident.hpp"

bool Resident::work;
const int Resident::threadSleep;

Resident::Resident(short id, Apartment *apartment, Office *office, Road *road, bool* officeWork, short bedImportant, short bathImportant, short eatImportant)
{
    this->apartment = apartment;
    this->apartmentId = apartment->getId();
    this->road = road;
    this->office = office;
    this->officeWork = officeWork;

    floors = office->getFloors();
    floor = 0;
    work = true;
    this->id = id;
    bedPrior = 0;
    bathPrior = 0;
    eatPrior = 0;

    this->bedImportant = bedImportant;
    this->bathImportant = bathImportant;
    this->eatImportant = eatImportant;

    couple = id % 2 == 0 ? id + 1 : id - 1;
}


short Resident::getId()
{
    return id;
}
void Resident::live(Resident *r)
{

    r->bedPrior = 30;
    r->bathPrior = 30;
    r->eatPrior = 20;
    short counter = 0;
    bool lastOffice = false;
    while (work)
    {
        float rnd = (rand() % 5 + 8) / 10.0;
        if(!*(r->officeWork)){
            if(lastOffice){
                r->bedPrior = 100;
                r->bathPrior = 70;
                r->eatPrior = 40;
                lastOffice = false;
            }
            if (r->bedPrior > r->bedImportant && r->apartment->startUsingBed(r->id, r->couple))
            {
                counter = 0;
                float sleepTime = r->apartment->bedTime() * rnd;
                r->isSleepy = false;
                r->action = "Sleeping";
                while (counter < sleepTime)
                {
                    counter++;
                    r->progress = (float)counter / (float)sleepTime * 100.0;
                    usleep(threadSleep);
                }
                r->apartment->stopUsingBed(r->id, r->couple);
                r->bedPrior = 0;
            }
            else if (r->bathPrior > r->bathImportant && r->apartment->startUsingBath())
            {
                float bathTime = r->apartment->bathTime() * rnd;
                r->isSmelly = false;
                r->action = " Shower ";
                while (counter < bathTime)
                {
                    counter++;
                    r->progress = (float)counter / (float)bathTime * 100.0;
                    usleep(threadSleep);
                }
                r->apartment->stopUsingBath();
                counter = 0;
                r->bathPrior = 0;
            }
            else if (r->eatPrior > r->eatImportant && r->apartment->startUsingKitchen())
            {
                counter = 0;
                float eatTime = r->apartment->kitchenTime() * rnd;
                r->isHungry = false;
                r->action = " Eating ";
                while (counter < eatTime)
                {
                    counter++;
                    r->progress = (float)counter / (float)eatTime * 100.0;
                    r->progress = r->progress > 100 ? 100.0 : r->progress;
                    usleep(threadSleep);
                    eatTime = r->apartment->kitchenTime() * rnd;
                }
                r->apartment->stopUsingKitchen();
                counter = 0;
                r->eatPrior = 0;
            }
            else
            {
                r->action = "Waiting ";
                r->progress = 0;
            }
            r->bedPrior++;
            r->eatPrior++;
            r->bathPrior++;

            r->isHungry = r->eatPrior >= r->eatImportant;
            r->isSmelly = r->bathPrior >= r->bathImportant;
            r->isSleepy = r->bedPrior >= r->bedImportant;
            usleep(threadSleep);
        }
        else{
            lastOffice = true;
            r->useRoad("Drive to");
            r->goToOffice(r);
            r->useRoad("Drive fr");
        }
    }
}

void Resident::useRoad(string action){
    this->action = action;
    road->hitTheRoad();
    int howLong = road->howLong();
    for (int i = 0; i < howLong; i += PERIOD)
    {
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
        this->progress = (float)i / (float)howLong * 100.0f;
    }
    this->progress = 100.0f;
    road->leaveTheRoad();
}



void Resident::goToOffice(Resident* r)
{
    r->action = " Search ";
    while (!r->office->canIQ(r->floor, r->id))
    {
        if(!(*r->officeWork)){
            return;
        }
        for (int i = 0; i < SEARCHING; i++)
        {
            r->progress = (float)(i + 1) / (float)SEARCHING * 100.0f;
            this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
        }
        r->floor = (r->floor + 1) % r->floors;
        if (r->office->canIUseLift())
        {
            r->action = "In Lift" + to_string(r->floor);
            r->office->liftInUse();
            for (int i = 0; i < LIFT; i++)
            {
                r->progress = (float)(i + 1) / (float)LIFT * 100.0f;
                this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
            }
            r->office->freeLift();
        }
        else
        {
            r->action = " Stairs" + to_string(r->floor);
            for (int i = 0; i < STAIRS; i++)
            {
                r->progress = (float)(i + 1) / (float)STAIRS * 100.0f;
                this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
            }
        }
    }

    r->action = " Sitting";
    for (int i = 0; i < SEAT; i++)
    {
        if(!(*r->officeWork)){
            return;
        }
        r->progress = (float)(i + 1) / (float)SEAT * 100.0f;
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
    }
    
    r->action = "Waiting ";
    while(r->office->stillInQ(id,floor)){
        if(!(*r->officeWork)){
            return;
        }
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
    }

    r->action = " Strugle";
    for (int i = 0; i < STRUGLE; i++)
    {
        if(!(*r->officeWork)){
            return;
        }
        r->progress = (float)(i + 1) / (float)STRUGLE * 100.0f;
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
    }
}

Resident *Resident::create(short id, Apartment *apartment, Office *office, Road *road, bool* officeWork)
{
    Resident *r = new Resident(id, apartment, office, road, officeWork);
    new std::thread(Resident::live, r);
    return r;
    //r->live();
    //return r->kill;
}

void Resident::kill()
{
    work = false;
}