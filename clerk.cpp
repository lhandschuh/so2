#include "clerk.hpp"

Clerk::Clerk(short id, Office *office, bool* open)
{
    this->open = open;
    this->office = office;
    this->progress = 0;
    floors = office->getFloors();
    coffeeFloor = office->getCoffeeFloor();
    this->id = id;
    floor = 0;
}

void Clerk::live(Clerk *c)
{
    while (*(c->open))
    {
        c->changeFloor(c->coffeeFloor, c);
        c->action = "waiting    ";
        while (!c->office->giveMeCoffee())
        {
            c->progress = 0;
            this_thread::sleep_for(std::chrono::milliseconds(PERIOD+c->random()));
        }

        c->action = "drinking   ";
        for(int i=0;i<DRINKING;i++){
            c->progress = (float)(i+1)/(float)DRINKING * 100.0f;
            this_thread::sleep_for(std::chrono::milliseconds(PERIOD+c->random()));
        }

        c->action = "searching  ";
        while (!c->office->canISeat(c->floor))
        {
            for(int i=0;i<SEARCHING;i++){
                c->progress = (float)(i+1)/(float)SEARCHING * 100.0f;
                this_thread::sleep_for(std::chrono::milliseconds(PERIOD+c->random()));
            }
            c->changeFloor((c->floor + 1) % c->floors,c);
        }

        c->action = "sitting    ";
        for(int i=0;i<SEAT;i++){
            c->progress = (float)(i+1)/(float)SEAT * 100.0f;
            this_thread::sleep_for(std::chrono::milliseconds(PERIOD+c->random()));
        }

        int i = 0;
        while (!c->office->serve(c->floor))
        {
            c->action = "working    ";
            c->progress = (float)(i+1)/(float)WORKING * 100.0f;
            this_thread::sleep_for(std::chrono::milliseconds(PERIOD+c->random()));
            i++;
            if(i%2 == 0){
                c->office->releaseSeat(c->floor);
                c->changeFloor((c->floor + 1) % c->floors,c);
                while(!c->office->canISeat(c->floor)){
                    c->changeFloor((c->floor + 1) % c->floors,c);
                }
            }
        }
       c->office->releaseSeat(c->floor);
    }
    c->action = "i am free  ";
    c->progress = 0;
    while(!*(c->open)){
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD*5));
    }
    c->live(c);
}

void Clerk::changeFloor(int nextFloor,Clerk* c)
{
    if (nextFloor != c->floor)
        {
            if (c->office->canIUseLift())
            {
                c->action = "in lift" + to_string(nextFloor) + "   ";
                c->office->liftInUse();
                for(int i=0; i<LIFT; i++){
                    c->progress = (float)(i+1)/(float)LIFT * 100.0f;
                    this_thread::sleep_for(std::chrono::milliseconds(PERIOD+c->random()));
                }
                c->office->freeLift();
            }
            else
            {
                c->action = "on stairs" + to_string(nextFloor) + " ";
                for(int i=0; i<STAIRS; i++){
                    c->progress = (float)(i+1)/(float)STAIRS * 100.0f;
                    this_thread::sleep_for(std::chrono::milliseconds(PERIOD+c->random()));
                }
            }
            c->floor = nextFloor;
        }
}

int Clerk::random(){
    return rand()%101-100;
}

Clerk *Clerk::create(short id, Office *office, bool* open)
{
    Clerk *c = new Clerk(id, office, open);
    new std::thread(Clerk::live, c);
    return c;
    //c->live();
    //return c>kill;
}

short Clerk::getId(){
    return id;
}