#pragma once

#include "include.hpp"

#define timeForAction 500000


class Apartment{
private:
    short id;

    short kitchenPlaces;
    short baths;
    short beds;

    mutex kitchen, bathroom, bedroom;
    vector<short> bedUsers;
    
public:

    Apartment(short id, short baths = 2, short beds = 3, short kitchenPlaces = 5);

    short getId();
    short kitchenUsage;
    short bathsUsage;
    short bedsUsage;

    bool startUsingKitchen();
    void stopUsingKitchen();
    short kitchenTime();

    bool startUsingBath();
    void stopUsingBath();
    short bathTime();

    bool startUsingBed(short id, short coupleId);
    void stopUsingBed(short id, short coupleId);
    short bedTime();
};