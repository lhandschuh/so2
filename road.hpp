#include "include.hpp"

#ifndef _ROAD_HPP_
#define _ROAD_HPP_ 

class Road{

private:

int drivers;
mutex drive;

public:

Road();
int howLong();
void hitTheRoad();
void leaveTheRoad();

};


#endif