#include "road.hpp"
#include "include.hpp"
#include "office.hpp"

#ifndef _PROVIDER_HPP_
#define _PROVIDER_HPP_ 

#define EMPTY 50
#define MAX_CAPASITY 300
#define MIN_CAPASITY 100
#define LONGEST_DEL 5000
#define REST 10
#define CHECK 5

class Provider{

private:

short id;
bool need;
bool* open;
Road* road;
Office* office;

static void live(Provider* p);

public:
string action;
float progress;

Provider(short id, Road* road, Office* office, bool* open);
void deliverCoffee();
void hitTheRoad();
short getId();
void closed(Provider* p);
static Provider* create(short id,Road* road, Office* office, bool* open);

};

#endif