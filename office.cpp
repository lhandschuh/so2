#include "office.hpp"

Office::Office(int numberOfFloors, int numberOfSeats,int coffeeFloor){
    floors = numberOfFloors;
    lift = true;
    maxSeats = numberOfSeats;
    seat = new int[floors];
    whereIsCoffee = coffeeFloor;
    queue = new list<short>[floors];
    coffee = 50;
    storeroom = 0;

    for(int i=0;i<floors;i++){
        seat[i] = 0;
    }
}

int Office::checkCoffee(){
    dispenser.lock();
    int howMany = coffee;
    dispenser.unlock();
    return howMany;
}

string Office::getQs(int floor){
    QQ.lock();
    string qs = to_string(queue[floor].size());
    QQ.unlock();
    return qs;
}

string Office::getCs(int floor){
    window.lock();
    string cs = to_string(seat[floor]);
    window.unlock();
    return cs;
}

void Office::loadCoffee(){
    dispenser.lock();
    store.lock();
    if(coffee + storeroom > MAX_COFFEE){
        int howMany = MAX_COFFEE - coffee;
        coffee = MAX_COFFEE;
        storeroom-= howMany;
    }
    else{
        coffee+= storeroom;
        storeroom = 0;
    }
    dispenser.unlock();
    store.unlock();
}

bool Office::giveMeCoffee(){
    dispenser.lock();
    if(coffee!=0){
        coffee--;
        dispenser.unlock();
        return true;
    }
    else{
        dispenser.unlock();
        return false;
    }
}

bool Office::canIQ(int floor, short id){
    QQ.lock();
    if(queue[floor].size() == maxSeats){
        QQ.unlock();
        return false;
    }
    else{
        queue[floor].push_back(id);
        QQ.unlock();
        return true;
    }
}

bool Office::stillInQ(short id, int floor){
    QQ.lock();
    for(list<short>::iterator it = queue[floor].begin(); it!=queue[floor].end();it++){
        if(*it == id){
            QQ.unlock();
            return true;
        }
    }
    QQ.unlock();
    return false;
}

bool Office::serve(int floor){
    QQ.lock();
    if(queue[floor].size() == 0){
        QQ.unlock();
        return false;
    }
    else{
        queue[floor].pop_front();
        QQ.unlock();
        return true;
    }
}

bool Office::canISeat(int floor){
    window.lock();
    if(seat[floor]==maxSeats){
        window.unlock();
        return false;
    }
    else{
        seat[floor]++;
        window.unlock();
        return true;
    }
}

bool Office::canIUseLift(){
    elevator.lock();
    bool canIUse = lift;
    elevator.unlock();
    return canIUse;
}

void Office::liftInUse(){
    elevator.lock();
    lift = false;
    elevator.unlock();
}

void Office::freeLift(){
    elevator.lock();
    lift = true;
    elevator.unlock();
}

void Office::releaseSeat(int floor){
    window.lock();
    seat[floor]--;
    window.unlock();
}

int Office::checkStoreroom(){
    store.lock();
    int howMany = storeroom;
    store.unlock();
    return howMany;
}


void Office::fillStoreroom(int coffee){
    store.lock();
    storeroom+=coffee;
    store.unlock();
}

int Office::getFloors(){
    return floors;
}

int Office::getCoffeeFloor(){
    return whereIsCoffee;
}