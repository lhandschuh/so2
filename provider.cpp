#include "provider.hpp"

Provider::Provider(short id, Road* road, Office* office, bool* open){
    this->road = road;
    this->open = open;
    this->office = office;
    this->id = id;
    this->progress = 0;
    this->action = "";
}

void Provider::live(Provider* p){
    while(*(p->open)){
        p->action = " checking ";
        p->progress = 0;
        while(p->office->checkStoreroom() > EMPTY){
            for(int i=0; i<CHECK; i++){
                if(!*(p->open)){
                    p->closed(p);
                }
            p->progress = (float)(i+1)/(float)CHECK*100.0f;
            this_thread::sleep_for(chrono::milliseconds(PERIOD));
        }
        }
        p->hitTheRoad();
        p->deliverCoffee();
        p->hitTheRoad();
        p->action = " resting  ";
        for(int i=0; i<REST;i++){
            p->progress = (float)(i+1)/(float)REST*100.0f;
            this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
        }
    }
    p->closed(p);
}

void Provider::closed(Provider* p){
    p->action = " i am free  ";
    p->progress = 0;
    while(!*(p->open)){
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD*5));
    }
    p->live(p);
}

short Provider::getId(){
    return id;
} 

void Provider::deliverCoffee(){
    this->action = " deliverin";
    int coffee = rand()%(MAX_CAPASITY-MIN_CAPASITY+1) + MIN_CAPASITY;
    int howlong = (float)coffee/(float)MAX_CAPASITY*LONGEST_DEL;
    for(int i=0;i<howlong;i+=PERIOD){
        this->progress = (float)i/(float)howlong*100.0f;
         this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
    }
    office->fillStoreroom(coffee);
}

void Provider::hitTheRoad(){
    road->hitTheRoad();
    this->action = " driving  ";
    int howLong = road->howLong();
    for (int i=0;i<howLong; i+=PERIOD){
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
        this->progress = (float)i/(float)howLong*100.0f;
    }
    this->progress = 100.0f;
    road->leaveTheRoad();
}

Provider* Provider::create(short id,Road* road, Office* office, bool* open){
    Provider* p = new Provider(id,road,office,open);
    new std::thread(Provider::live, p);
    return p;
}

