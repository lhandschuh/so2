#include "include.hpp"
#include "provider.hpp"
#include "office.hpp"
#include "road.hpp"

#ifndef _CARETAKER_HPP_
#define _CARETAKER_HPP_ 

#define TOO_FEW 25
#define CHECKING 5
#define RESTING 10
#define LOADING 4

class Caretaker{

private:

short id;
Office* office;
bool* open;

static void live(Caretaker* c);

public:

string action;
float progress;

short getId();
Caretaker(short id,Office* office,bool* open);
static Caretaker* create(short id, Office* office,bool* open);
void closed(Caretaker* c);

};

#endif