#include "include.hpp"
#include "apartment.hpp"
#include "resident.hpp"
#include "office.hpp"
#include "clerk.hpp"
#include "provider.hpp"
#include "caretaker.hpp"
#include "road.hpp"

using namespace std;

vector<Apartment *> apartments;
vector<Resident *> residents;
vector<Clerk *> clerks;
vector<Provider *> providers;
vector<Caretaker *> caretakers;
Road *road;
Office *office;
bool* open;

bool running = true;

void onExit();
void updateScreen();

void init(short apartmentsNumber = 1, short minResidents = 10, short maxResidents = 10, short clerksNumber = 10, short providerNumber = 1, short caretakerNumber = 1)
{
    int tmpResidents = minResidents;
    apartments = vector<Apartment *>();
    residents = vector<Resident *>();
    clerks = vector<Clerk *>();
    providers = vector<Provider *>();
    caretakers = vector<Caretaker *>();
    office = new Office(3, 3, 1);
    road = new Road();

    for (short i = 0; i < apartmentsNumber; i++)
    {
        Apartment *a = new Apartment(i);
        apartments.push_back(a);
        for (short j = 0; j < tmpResidents; j++)
        {
            residents.push_back(Resident::create(j, a, office, road, open));
        }
        tmpResidents += maxResidents;
    }

    for (short i = 0; i < providerNumber; i++)
    {
        providers.push_back(Provider::create(i, road, office, open));
    }

    for (short i = 0; i < caretakerNumber; i++)
    {
        caretakers.push_back(Caretaker::create(i, office, open));
    }

    for (short i = 0; i < clerksNumber; i++)
    {
        clerks.push_back(Clerk::create(i, office, open));
    }

    new thread(updateScreen);
    char c = getch();
    (*residents.at(0)).kill();
    running = false;
    onExit();
}

void onExit()
{
    for (auto it = apartments.begin(); it != apartments.end(); it++)
    {
        delete *it;
    }
    for (auto it = residents.begin(); it != residents.end(); it++)
    {
        delete *it;
    }
    for (auto it = clerks.begin(); it != clerks.end(); it++)
    {
        delete *it;
    }
    for (auto it = providers.begin(); it != providers.end(); it++)
    {
        delete *it;
    }
    for (auto it = caretakers.begin(); it != caretakers.end(); it++)
    {
        delete *it;
    }
    delete office;
    delete road;
}

void updateScreen()
{
    while (true)
    {
        vector<const char *> s(1, " ID watku       Praca    P  S J M");
        for (auto it = residents.begin(); it != residents.end(); it++)
        {
            string resident = "Resident" + (((**it).getId() < 10) ? "0" + to_string((**it).getId()) : to_string((**it).getId())) + "-" + to_string((**it).apartmentId) + "   " + (**it).action + (((**it).progress < 100.0) ? "  " : " ") + (((**it).progress < 10.0) ? "0" : "") + to_string((int)((**it).progress)) + " " + to_string((int)(**it).isSleepy) + " " + to_string((int)(**it).isHungry) + " " + to_string((int)(**it).isSmelly) + "     ";

            char *tab = new char[resident.length() + 1];
            strcpy(tab, resident.c_str());
            s.push_back((const char *)tab);
        }

        attron(COLOR_PAIR(1));
        for (int i = 0; i < s.size(); i++)
        {
            mvprintw(i, 0, s.at(i));
        }
        attroff(COLOR_PAIR(1));

        vector<const char *> s1(1, " ID apart    L P K");
        for (auto it = apartments.begin(); it != apartments.end(); it++)
        {
            Apartment *a = *it;
            string apartment = "Apartment" + to_string((*a).getId()) + "   " + to_string((*a).bedsUsage) + " " + to_string((*a).bathsUsage) + " " + to_string((*a).kitchenUsage) + "     ";
            char *tab = new char[apartment.length() + 1];
            strcpy(tab, apartment.c_str());
            s1.push_back(tab);
        }

        attron(COLOR_PAIR(2));
        for (int i = 0; i < s1.size(); i++)
        {
            mvprintw(i, 40, s1.at(i));
        }
        attroff(COLOR_PAIR(2));

        vector<const char *> s2(1, "ID watku   Praca       P");
        for (auto it = clerks.begin(); it != clerks.end(); it++)
        {
            string clerk = "Clerk" + (((**it).getId() < 10) ? "0" + to_string((**it).getId()) : to_string((**it).getId())) + "   " + (**it).action + (((**it).progress < 100.0) ? "  " : " ") + (((**it).progress < 10.0) ? "0" : "") + to_string((int)((**it).progress)) + " ";

            char *tab = new char[clerk.length() + 1];
            strcpy(tab, clerk.c_str());
            s2.push_back((const char *)tab);
        }

        attron(COLOR_PAIR(3));
        for (int i = 0; i < s2.size(); i++)
        {
            mvprintw(i, 65, s2.at(i));
        }
        attroff(COLOR_PAIR(3));

        vector<const char *> s3(1, "ID watku      Praca       P");
        for (auto it = providers.begin(); it != providers.end(); it++)
        {
            string provider = "Provider" + (((**it).getId() < 10) ? "0" + to_string((**it).getId()) : to_string((**it).getId())) + "   " + (**it).action + (((**it).progress < 100.0) ? "  " : " ") + (((**it).progress < 10.0) ? "0" : "") + to_string((int)((**it).progress)) + "  ";

            char *tab = new char[provider.length() + 1];
            strcpy(tab, provider.c_str());
            s3.push_back((const char *)tab);
        }

        for (auto it = caretakers.begin(); it != caretakers.end(); it++)
        {
            string caretaker = "Caretaker" + (((**it).getId() < 10) ? "0" + to_string((**it).getId()) : to_string((**it).getId())) + "   " + (**it).action + (((**it).progress < 100.0) ? "  " : " ") + (((**it).progress < 10.0) ? "0" : "") + to_string((int)((**it).progress)) + "  ";

            char *tab = new char[caretaker.length() + 1];
            strcpy(tab, caretaker.c_str());
            s3.push_back((const char *)tab);
        }


        attron(COLOR_PAIR(4));
        for (int i = 0; i < s3.size(); i++)
        {
            mvprintw(i, 95, s3.at(i));
        }

        attroff(COLOR_PAIR(4));

        vector<const char *> s4(1, "Zasob    stan");
        int x = office->checkCoffee();
        string data ="";
        if(x<100){
                data = " ";
            if(x<10){
                data = "  ";
            }
        }
        string row = "automat  " + data + to_string(office->checkCoffee()) + "  ";
        char *tab = new char[row.length() + 1];
        strcpy(tab, row.c_str());
        s4.push_back((const char *)tab);
        x = office->checkStoreroom();
        if(x<100){
                data = " ";
            if(x<10){
                data = "  ";
            }
        }
        row = "magazyn  " + data + to_string(office->checkStoreroom()) + "  ";
        tab = new char[row.length() + 1];
        strcpy(tab, row.c_str());
        s4.push_back((const char *)tab);
        row = "winda    ";
        if(office->canIUseLift()){
            row+="wolna ";
        }
        else{
            row+="zajeta";
        }
        tab = new char[row.length() + 1];
        strcpy(tab, row.c_str());
        s4.push_back((const char *)tab);
        int size = office->getFloors();
        for(int i=0;i<size;i++){
            row = "pietro"+to_string(i)+"   " + office->getCs(i) + "/" + office->getQs(i);
            tab = new char[row.length() + 1];
            strcpy(tab, row.c_str());
            s4.push_back((const char *)tab);
        }


        attron(COLOR_PAIR(2));
        for (int i = 0; i < s4.size(); i++)
        {
            mvprintw(i, 130, s4.at(i));
        }
        attroff(COLOR_PAIR(2));

        refresh();
        usleep(PERIOD * 1000);
    }
}

static void officeState(){
    for(int i=0; running; i++){
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD));
        if(i%120 == 30){
            *open = true;
        }
        else if(i%120 == 90){
            *open = false;
        }
    }
}

int main()
{
    srand(time(NULL));
    initscr();
    noecho();
    open = new bool();
    *open = false;
    start_color();
    init_pair(1, COLOR_BLUE, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_RED, COLOR_BLACK);
    init_pair(4, COLOR_YELLOW, COLOR_BLACK);

    new thread(officeState);
    thread(init, 3, 7, 3, 10, 1, 1).join();

    endwin();
}