#include "caretaker.hpp"

Caretaker::Caretaker(short id, Office *office, bool* open)
{
    this->open = open;
    this->id = id;
    this->office = office;
    this->progress = 0;
    this->action = "";
}

short Caretaker::getId()
{
    return id;
}

void Caretaker::live(Caretaker *c)
{
    while (*(c->open))
    {
        c->action = "checking ";
        c->progress = 0;
        while (c->office->checkCoffee() > TOO_FEW)
        {
            for (int i = 0; i < CHECKING; i++)
            {
                if(!*(c->open)){
                    c->closed(c);
                }
                c->progress = (float)(i + 1) / (float)CHECKING * 100.0f;
                this_thread::sleep_for(chrono::milliseconds(PERIOD));
            }
        }
        while (c->office->checkStoreroom() == 0)
        {
            c->action = "waiting  ";
            this_thread::sleep_for(chrono::milliseconds(PERIOD));
        }
        c->action = "loading  ";
        for (int i = 0; i < LOADING; i++)
        {
            c->progress = (float)(i + 1) / (float)LOADING * 100.0f;
            this_thread::sleep_for(chrono::milliseconds(PERIOD));
        }
        c->office->loadCoffee();
        c->action = "resting  ";
        for (int i = 0; i < RESTING; i++)
        {
            c->progress = (float)(i + 1) / (float)RESTING * 100.0f;
            this_thread::sleep_for(chrono::milliseconds(PERIOD));
        }
    }
    c->closed(c);
}

void Caretaker::closed(Caretaker* c){
    c->action = "i am free  ";
    c->progress = 0;
    while(!*(c->open)){
        this_thread::sleep_for(std::chrono::milliseconds(PERIOD*5));
    }
    c->live(c);
}

Caretaker *Caretaker::create(short id, Office *office,bool* open)
{
    Caretaker *c = new Caretaker(id, office,open);
    new std::thread(Caretaker::live, c);
    return c;
}
