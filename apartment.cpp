#include "apartment.hpp"

Apartment::Apartment(short id, short baths, short beds, short kitchenPlaces){
    this->id = id;
    kitchenUsage = 0;
    bathsUsage = 0;
    bedsUsage = 0;
    this->baths = baths;
    this->beds = beds;
    this->kitchenPlaces = kitchenPlaces;
    this->bedUsers = vector<short>();
}

short Apartment::getId(){
    return id;
}

bool Apartment::startUsingKitchen(){
    bool ret = false;
    kitchen.lock();
    if (kitchenUsage < kitchenPlaces){
        kitchenUsage+=1;
        ret = true;
    }
    kitchen.unlock();
    return ret;
}

void Apartment::stopUsingKitchen(){
    kitchen.lock();
    kitchenUsage -= 1;
    kitchen.unlock();
}

short Apartment::kitchenTime(){
    return kitchenUsage + 2;
}

bool Apartment::startUsingBath(){
    bool ret;
    bathroom.lock();
    if(bathsUsage < baths){
        bathsUsage++;
        ret = true;
    } else {
        ret = false;
    }
    bathroom.unlock();
    return ret;
}

void Apartment::stopUsingBath(){
    bathroom.lock();
    bathsUsage--;
    bathroom.unlock();
}

short Apartment::bathTime(){
    return 6;
}

bool Apartment::startUsingBed(short id, short coupleId){
    bool ret = false;
    bedroom.lock();
    if(find(bedUsers.begin(), bedUsers.end(), coupleId) != bedUsers.end()){
        bedUsers.push_back(id);
        ret = true;
    } else if (beds > bedsUsage) {
        bedUsers.push_back(id);
        bedsUsage++;
        ret = true;
    }
    bedroom.unlock();
    return ret;
}

void Apartment::stopUsingBed(short id, short coupleId){
    bedroom.lock();
    vector<short>::iterator it = find(bedUsers.begin(), bedUsers.end(), id);
    vector<short>::iterator cit = find(bedUsers.begin(), bedUsers.end(), coupleId);
    if(cit == bedUsers.end()){
        bedsUsage--;
    }
    *it = bedUsers[bedUsers.size() -1];
    bedUsers.pop_back();
    bedroom.unlock();
}

short Apartment::bedTime(){
    return 20;
}