#include "road.hpp"

Road::Road(){
    drivers = 0;
}

int Road::howLong(){
    drive.lock();
    int howMany = 2000+drivers*200;
    drive.unlock();
    return howMany;
}

void Road::hitTheRoad(){
    drive.lock();
    drivers++;
    drive.unlock();
}

void Road::leaveTheRoad(){
    drive.lock();
    drivers--;
    drive.unlock();
}