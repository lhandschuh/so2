#include "include.hpp"
#include "office.hpp"

#ifndef _CLERK_HPP_
#define _CLERK_HPP_ 

#define DRINKING 4
#define WORKING 20
#define CHANGING 2

class Clerk{

private:

bool* open;
short id;
int floor;
int floors;
int coffeeFloor;
Office* office;

static void live(Clerk* c);

public:
float progress;
string action;
Clerk(short id, Office *office, bool* open);
void changeFloor(int nextFloor, Clerk* c);
short getId();
static Clerk* create(short id, Office* office, bool* open);
int random();

};

#endif