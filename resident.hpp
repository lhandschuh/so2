#pragma once

#include "apartment.hpp"
#include "office.hpp"
#include "road.hpp"
#include "include.hpp"

#define timeForAcrion 500000
#define STRUGLE 4

using namespace std;

typedef void (* func)();

class Resident {
private:
    Apartment* apartment;
    Office* office;
    Road* road;
    short id;
    static bool work;
    short bedPrior;
    short bathPrior;
    short eatPrior;

    short bedImportant;
    short bathImportant;
    short eatImportant;

    bool* officeWork;

    short floor;
    short floors;

    static const int threadSleep = 500000;
    short couple;

    static void live(Resident* r);
    void useRoad(string acrion);
    void goToOffice(Resident* r);
public:
    Resident(short id, Apartment* apartment, Office* office,Road* road, bool* officeWork, short bedImportant = 120, short bathImportant = 100, short eatImportant = 30);
    short getId();
    short apartmentId;
    float progress;
    string action;
    bool isHungry;
    bool isSmelly;
    bool isSleepy;
    static Resident* create(short id, Apartment* apartment, Office* office,Road* road, bool* officeWork);
    void kill();
};
